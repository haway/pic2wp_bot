#!/bin/python
# coding:utf-8

import configparser, re, telegram, requests, json, hashlib
from datetime import date
from flask import Flask, request
from telegram import KeyboardButton, ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup

config = configparser.ConfigParser()
config.read('config.ini')

app = Flask(__name__)
bot = telegram.Bot(token=(config['TELEGRAM']['ACCESS_TOKEN']))
USER_PROFILE_DIR = config['SETTINGS']['USER_PROFILE_DIR']
FILE_ID_DIR = config['SETTINGS']['FILE_ID_DIR']
ENV = config['SETTINGS']['ENV']
NewebPay = 'https://core.newebpay.com/EPG/30cmgg/9ZgOUZ'
md5 = hashlib.md5()

def UpdateUserJson( chatid=None, ujson=None ):
	if chatid == None:
		return None

	print( " + UpdateUserJson -> "+USER_PROFILE_DIR+str( chatid ) )
	f = open( USER_PROFILE_DIR+"/user_profile_"+str(chatid), "w" )
	f.write(json.dumps( ujson ))
	f.close()

	return 1

def ReadUserJson( chatid=None ):
	if chatid == None:
		return None

	print( " + ReadUserJson -> "+USER_PROFILE_DIR+"/user_profile_"+str( chatid ) )
	try:
		f = open( USER_PROFILE_DIR+"/user_profile_"+str(chatid), "r" )
		return json.loads( f.readline()	)
	except:
		print( "  +-> return None" )
		return None

	return None

app.route('/hello')
def hello():
	return "Hello"

@app.route('/telegram', methods=['POST'])
def handle_update():
	update = telegram.Update.de_json(request.get_json(force=True), bot)
	#print( "request:" + str( request.get_json( force=True) ) )
	print( "update:" + str( update ) )

	if request.method == "POST":
		try:
			update.message.chat.id
		except:
			print( "NO update" )
		else:
			chatid = update.message.chat.id
			user_profile = ReadUserJson( chatid )
			print( "Receive POST with chatid {}".format( str(chatid) ) )

		try:
			update.message.text
		except:
			print( "No text" )
		else:
			if update.message.text:
				text = str( update.message.text )
				stext = text
				print( "Receive Text:"+"["+ENV+"]"+stext )
				if re.match( r'\/?[sS][tT][aA][Rr][tT]$', text ):
					stext = "自動上傳圖片到 WordPress 的聊天機器人"
					stext += "\n註冊 WordPress 帳號：/help"
					stext += "\n如何取得 token: https://haway.30cm.gg/wordpress-api-key"
					stext += "\n邀請朋友使用機器人： t.me/photo2wp_bot"

				if re.match( r'\/?[hH][Ee][Ll][Pp]', text ):
					stext = "/web (網址) (WordPress User) (token)"
					stext += "\n如何取得 token: https://haway.30cm.gg/wordpress-api-key"
					stext += "\n邀請朋友使用機器人： t.me/photo2wp_bot"

				if re.match( r'\/?[Ee][Nn][Vv]', text ):
					stext = str( ENV )

				if re.match( r'\/?[uU][pP][lL][oO][aA][dD]', text ):
					ProcessUpload( chatid )
					stext = "Upload 結束, 贊助作者：" + NewebPay
			
				if re.match( r'\/?[Cc][Aa][Nn][Cc][Ee][lL]', text ):
					CancelUpload( chatid )
					stext = ""

				command = re.match( r'^\/(\w+) ([\w:\/\.]+) ([\w@.-_]+) ([\w ]+)$', text )
				if command and command.group(1) == 'web':
					web = command.group(2)
					user = command.group(3)
					token = command.group(4)
					stext = "Chatid:"+str(chatid)
					stext += "\n註冊資料，網站："+web
					stext += "\n註冊資料，User："+user
					stext += "\n註冊資料，Token："+token
					user_profile = { "chatid": chatid, "web": web, "user": user, "token": token }
					if 	UpdateUserJson( chatid, user_profile ):
						stext = "註冊成功"
					else:
						stext = "註冊失敗"

				if re.match( r'\/?[Ww][Ee][Bb]', text ):
					stext = user_profile['web']	

				if stext != "":
					bot.send_message(chat_id=chatid, text=stext)

		try: 
			update.message.photo
		except:
			print( "NO update.message.photo" )
		else:
			if update.message.photo:
				print( "Receive Photo" )
				#print( "Message ID:" + str( update.message.message_id ) )
				size = 0
				for aphoto in update.message.photo:
					if aphoto.file_size > size:
						file_id = aphoto.file_id
				
				print( " +-> "+FILE_ID_DIR+"/"+str( file_id ) )
				f = open( FILE_ID_DIR+"/"+str(update.message.chat.id), "a" )
				f.write(file_id + "\n")
				f.close()
				#keyboard = [[InlineKeyboardButton("是", callback_data='upload' ),
							#InlineKeyboardButton("否", callback_data='no')]]
				#reply_markup = InlineKeyboardMarkup(keyboard)
				#bot.send_message(chat_id=chatid, text='您要上傳這張圖片嗎？多張圖片只需回復最後一個 "是"', reply_markup=reply_markup)
				buttons = [[KeyboardButton("Upload"), KeyboardButton("Cancel")]]
				reply_markup = ReplyKeyboardMarkup(buttons, one_time_keyboard=True, resize_keyboard=True)
				bot.send_message(chat_id=chatid, text='收到一張圖片，請點選按鈕回覆', reply_markup=reply_markup )

		try:
			update.callback_query.data
		except:
			print( "NO callback" )
		else:
			# 回應的訊息
			print( "Receive Callback" )
			chatid = update.callback_query.message.chat.id 
			user_profile = ReadUserJson( chatid )
			print( " +-> chat id in callback:" + str(chatid) )
			print( " +-> user_profile:" + str( user_profile ) )
			if update.callback_query.data:
				if update.callback_query.data == 'upload':
					#bot.send_message(chat_id=chatid, text='圖片上傳中...')
					#asyncio.get_event_loop().run_until_complete( ProcessUpload( chatid ) )
					ProcessUpload( chatid )
				else:
					# 選擇否，不上傳圖片
					CancelUpload( chatid )

	return 'ok'

def DelPhoto( chatid ):
	with open( FILE_ID_DIR+"/"+str(chatid), 'w' ) as f:
		f.write("")

def CancelUpload( chatid ):
	DelPhoto( chatid )
	bot.send_message(chat_id=chatid, text='好的，圖片沒有上傳。')

def ProcessUpload( chatid ):
	user_profile = ReadUserJson( chatid )
	try:
		user_profile["token"]
	except:
		print( " +-> Can't found user_profile:"+str( chatid ) )
		bot.send_message(chat_id=chatid, text='請先註冊 /help' )
		return 'OK'

	bot.send_message(chat_id=chatid, text='圖片上傳中' )

	photo_upload_success = 0
	photo_upload_fail = 0
	# 選擇是，上傳圖片
	user = user_profile["user"]
	wtoken = user_profile["token"]
	web_api = user_profile["web"]+"/wp-json/wp/v2/media"
	today = date.today()
	print( " + -> POST Image: {}, user:{}, token:{}".format( web_api, user, wtoken ) )

	with open( FILE_ID_DIR+"/"+str(chatid)) as f:
		for fid in f:
			if fid.isspace():
				continue
			print( " +-> fid:"+ str(fid.strip() ) )
			photo = bot.get_file( fid.strip() )
			print( str( photo ) )
			response = requests.get( photo.file_path )
		
			md5.update(fid.encode("utf-8"))
			p10md5 = md5.hexdigest()
			imgname = today.strftime( '%Y%m%d%H_{}.jpeg'.format( p10md5[:10] ) )
			print( " +-> imgname:" + imgname )
			headers = {'Content-Type': 'image/jpg', 'Content-Disposition': 'attachment; filename=%s' % imgname }
			response = requests.post(web_api, headers=headers, data=response.content, auth=(user, wtoken) )

			print( " +-> Response of POST:" + str( response.status_code ) )
			if response.status_code == 201:
				photo_upload_success += 1
			else:
				photo_upload_fail += 1

	DelPhoto( chatid )

	if photo_upload_success > 0:
		bot.send_message(chat_id=chatid, text='{} 張圖片上傳成功！，備份圖片已刪除'.format( photo_upload_success ) )

	if photo_upload_fail > 0:
		bot.send_message(chat_id=chatid, text='{} 張圖片上傳失敗！，圖片已刪除，請重新傳送'.format( photo_upload_fail ) )

	
if __name__ == '__main__':
	app.run()
